<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page
	import="java.util.List, br.com.alura.gerenciador.modelo.Empresa"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lista Empresas JSP</title>
</head>
<body>

	<c:import url="logout-parcial.jsp" />
	<c:import url="form-nova-empresa-parcial.jsp" />

	Usu�rio Logado: ${usuarioLogado.login}
	<br />
	<br />
	<br />

	<c:if test="${not empty empresa}">
		Empresa ${empresa} Cadastrada com Sucessso!
	</c:if>

	Lista de Empresas:
	<br />

	<ul>
		<c:forEach items="${empresas}" var="empresa">
			<li>${empresa.nome} - <fmt:formatDate
					value="${empresa.dataAbertura}" pattern="dd/MM/yyyy" /> <a
				href="/gerenciador/entrada?action=MostraEmpresa&id=${empresa.id}">Editar</a>
				<a href="/gerenciador/entrada?action=RemoveEmpresa&id=${empresa.id}">Excluir</a>
			</li>
		</c:forEach>
	</ul>
</body>
</html>
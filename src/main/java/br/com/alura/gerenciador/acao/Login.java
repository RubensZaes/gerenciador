package br.com.alura.gerenciador.acao;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.alura.gerenciador.modelo.Banco;
import br.com.alura.gerenciador.modelo.Usuario;

public class Login implements Action {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		
		
		
		Banco banco = new Banco();
		Usuario usuario = banco.existeUsuario(login, senha);
		
		if (usuario != null) {
			System.out.println("Usu�rio OK!");
			System.out.println("Logando " + login);
			
			HttpSession session = request.getSession();
			session.setAttribute("usuarioLogado", usuario);
			
			return "redirect:entrada?action=ListaEmpresas";
		} else {
			System.out.println("Usu�rio Inv�lido!");
			return "redirect:entrada?action=LoginForm";
		}
	}

}
